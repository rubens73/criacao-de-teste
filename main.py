import csv

with open('casos de teste.csv', encoding='utf-8') as arquivo:

    tabela = csv.reader(arquivo, delimiter=';')
    next(tabela)

    for l in tabela:
        feature = l[1]
        cenario = l[2]
        dado = l[3]
        e = l[4]
        quando = l[5]
        entao = l[6]
        mas = l[7]

        nome_arquivo = feature + '.feature'

        texto_feature = '#language:pt \n\n' \
                'Cenario: ' + cenario + '\n' \
                '\tDado ' + dado + '\n' \
                '\tE ' + e + '\n' \
                '\tQuando ' + quando + '\n' \
                '\tEntao ' + entao + '\n' \
                '\tMas ' + mas + '\n'

        print(texto_feature)

        try:
            print('Rescrevendo')
            arquivo = open(nome_arquivo, 'r+', encoding='utf-8')
        except FileNotFoundError:
            print('Criando arquivo')
            arquivo = open(nome_arquivo, 'w+', encoding='utf-8')

        arquivo.writelines(texto_feature)
        arquivo.close()
